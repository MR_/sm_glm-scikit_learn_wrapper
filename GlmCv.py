from statsmodels.genmod.generalized_linear_model import GLM
from statsmodels.genmod import families
from statsmodels.tools.tools import add_constant
from sklearn.base import BaseEstimator, RegressorMixin

class GlmCv(BaseEstimator, RegressorMixin):
    """
    Customized GLM from Statsmodels to be integrated with Scikit-learn

    Arguments:
    - fit_intercept: default=True
        Whether the intercept should be estimated or not. If False, the data is assumed to be already centered.
    - family: default='Gaussian'
        The distribution family, available values: Binomial, Poisson, NegBinom, Gaussian, Gamma, InvGauss, Tweedie
    - link: default='identity'
        The link function, available values: Log, identity, Logit, Power, NegativeBinomial, CLogLog, LogLog, cauchy, 
                                             cloglog, loglog, inverse_power, inverse_squared
    - alpha_nb: default=1
        The ancillary parameter for the negative binomial distribution.
    - power: default=1
        The exponent of the power transform
    - method: default='irls'
        ‘IRLS’ for iteratively reweighted least squares. Others: 'lbfgs', 'bfgs', 'newton'
    - reg: default='False'
        use fit method or fit_regularized
    - alpha_reg: default=0
        The penalty weight. If a scalar, the same penalty weight applies to all variables in the model. 
        If a vector, it must have the same length as params, and contains a penalty weight for each coefficient
    - maxiter: default=100
        Maximum number of iterations
    - L1_wt: default=0
        Must be in [0, 1]. The L1 penalty has weight L1_wt and the L2 penalty has weight 1 - L1_wt
    - cnvrg_tol: default=0
        Coefficients below this threshold are treated as zero
    """
    
    __doc__ += RegressorMixin.__doc__   
    
    def __init__(self, fit_intercept=True, 
                 family='Gaussian', 
                 link='identity', 
                 alpha_nb=1, power=1,  
                 reg=False,
                 method='irls',
                 alpha_reg=0, 
                 maxiter=100, 
                 L1_wt=0, 
                 cnvrg_tol=0):
        
        self.fit_intercept = fit_intercept
        self.family = family
        self.link = link
        self.alpha_nb = alpha_nb
        self.power = power
        self.method = method
        self.reg = reg
        self.result = None
        self.alpha_reg = alpha_reg
        self.maxiter = maxiter
        self.L1_wt = L1_wt
        self.cnvrg_tol = cnvrg_tol
        self._links = {'Log': families.links.Log(),
                       'identity': families.links.identity(),
                       'Logit': families.links.Logit(),
                       'Power': families.links.Power(power=self.power),
                       'NegativeBinomial': families.links.NegativeBinomial(alpha=self.alpha_nb),
                       'CLogLog': families.links.CLogLog(),
                       'LogLog': families.links.LogLog(),
                       'cauchy': families.links.cauchy(),
                       'cloglog': families.links.cloglog(),
                       'loglog': families.links.loglog(),
                       'inverse_power': families.links.inverse_power(),
                       'inverse_squared': families.links.inverse_squared()}
        self._families = {'Binomial': families.Binomial(self._links.get(self.link)),
                          'Poisson': families.Poisson(self._links.get(self.link)),
                          'NegBinom.': families.NegativeBinomial(self._links.get(self.link)),
                          'Gaussian': families.Gaussian(self._links.get(self.link)),
                          'Gamma': families.Gamma(self._links.get(self.link)),
                          'InvGauss': families.InverseGaussian(self._links.get(self.link)),
                          'Tweedie': families.Tweedie(self._links.get(self.link))}
                                       
    def fit(self, X, y):
        if self.fit_intercept:
            X = add_constant(X, prepend=False)    
        
        if self.reg:
            if self.method == 'irls':
                self.method = 'bfgs'
                
            self.result = GLM(y, X, family=self._families.get(self.family)).fit_regularized(alpha=self.alpha_reg,
                                                                                            opt_method=self.method,
                                                                                            maxiter=self.maxiter,
                                                                                            L1_wt=self.L1_wt,
                                                                                            cnvrg_tol=self.cnvrg_tol)
        else:
            self.result = GLM(y, X, family=self._families.get(self.family)).fit(method=self.method)
                                                                                        
    def predict(self, X):
        if self.fit_intercept:
            X = add_constant(X, prepend=False)

        return  self.result.predict(X)
